# Manuel d'installation #

## Installation et configuration d'Eclipse ##
1. Installer Eclipse IDE for Java Developpers v4.4.1 (Luna)
2. Ouvrir le menu Help > Install New Software
3. Dans le champ "Work with", mettre "Luna - http://download.eclipse.org/releases/luna/"
4. Installer les add-ons suivants : 

* Eclipse Git Team Provider (et Source Code)
* Eclipse Java EE Developer Tools
* Eclipse Java Web Developer Tools
* Eclipse Tomcat Management Feature
* Eclipse Web Developer Tools
* JAX-WS DOM Tools
* JAX-WS Tools
* JSF Tools - Web Page Editor
* JST Server Adapters (et Extensions)
* m2e-wtp
* Task focused interface for Eclipse Git Team Provider
* WidowBuilder XML Core
* WST Server Adapters

## Installation du serveur Tomcat ##
Télécharger Apache Tomee v1.7.1 sur le site http://tomee.apache.org/downloads.html et le décompresser
Si les port 8005 ou 8080 sont déjà utilisés par une autre application, modifier le fichier conf > server.xml

## Import du projet ##
1. Ouvrir le menu File > Import
2. Selectionner Git > Projects from Git
3. Selectionner Clone URI
4. Indiquer l'URI du projet ainsi que ses identifiants BitBucket
5. Selectionner la branche "master"
6. Choisir un dossier de destination
7. Choisir Import Existing Projects
8. Selectionner PCMEditor et cliquer sur Finish

## Configuration du projet ##
1. Cliquer droit sur le projet, Maven > Update project
2. Cliquer droit sur le projet, Properties
3. Dans Java Build Path, onglet Libraries, cliquer sur Add JARS et selectionner src > lib > model.jar
4. Dans Deployement Assembly, cliquer sur Add et selectionner Java Build Path Entries > model.jar
5. Ouvrir la perspective JavaEE
6. Dans l'onglet Serveurs, cliquer sur le lien affiché
7. Selectionner Apache > Tomcat v7.0 Server
8. Cliquer sur Browse et selectionner le dossier d'installation de Tomee (/apache-tomee-plume-1.7.1) et cliquer sur Next>
9. Cliquer sur Add All puis sur Finish
10. Dans Web Project Setting, remplacer "projetglensai" par "PCMEditor" (attention à la casse)

## Lancer l'application ##
1. Dans Servers, cliquer droit sur le serveur puis sur Start
2. L'application est maintenant lancée. Allez sur http://localhost:8080/PCMEditor/ pour afficher la page d'accueil du serveur. Vous pouvez maintenant faire des requêtes.