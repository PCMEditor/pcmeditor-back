package fr.ensai.projet.interfaceBackFront;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.junit.Test;

import fr.ensai.projet.marshallPCM.XMLException;
import fr.ensai.projet.marshallPCM.XmlPCMBackToFront;
import fr.ensai.projet.metier.Cell;
import fr.ensai.projet.metier.Colonne;
import fr.ensai.projet.metier.Comment;
import fr.ensai.projet.metier.DataType;
import fr.ensai.projet.metier.MatriceTestEnDur;
import fr.ensai.projet.metier.PCM;

public class XmlPCMBackToFrontTest {


	@Test
	public void exportTest() {
		XmlPCMBackToFront xb2f = new XmlPCMBackToFront();
		File f = new File("testPCM.xml");
		MatriceTestEnDur mtdr = new MatriceTestEnDur();
		PCM pm = mtdr.getPcmTest();
		try {
			xb2f.export(pm, f);
		} catch (XMLException e) {
			// TODO Auto-generated catch block
			System.out.println("XmlPCMBackToFrontTest.exportTest() has failed; expect errors");
			e.printStackTrace();
		}


	}
	
	@Test
	public void exportTest2() throws Exception {

		Comment com = new Comment("Contenu","Erreur",true);

			JAXBContext jaxbContext = JAXBContext.newInstance(Comment.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
			jaxbMarshaller.marshal(com, System.out);
			
			
			
			
			
			ArrayList<Comment> coms = new ArrayList<Comment>();
			coms.add(com);
			Cell<String> cel = new Cell<String>(coms,"Sony Xperia L",DataType.Texte);
			
			JAXBContext jaxbContext2 = JAXBContext.newInstance(Cell.class);
			Marshaller jaxbMarshaller2 = jaxbContext2.createMarshaller();
			jaxbMarshaller2.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
			jaxbMarshaller2.marshal(cel, System.out);
		 

			ArrayList<Cell<?>> cx1 = new ArrayList<Cell<?>>();
			Cell<String> c31 = new Cell<String>(null,"Samsung Galaxy S4",DataType.Texte);
			cx1.add(c31);
			cx1.add(cel);
			Colonne col1 = new Colonne(DataType.Texte,cx1);
			
			JAXBContext jaxbContext3 = JAXBContext.newInstance(Colonne.class);
			Marshaller jaxbMarshaller3 = jaxbContext3.createMarshaller();
			jaxbMarshaller3.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
			jaxbMarshaller3.marshal(col1, System.out);

	}


}
