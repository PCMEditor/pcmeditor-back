package fr.ensai.projet.stat;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.BeforeClass;
import org.junit.Test;

public class StatLigneTest {
	StatLigne sl;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() throws JAXBException {
		sl = new StatLigne("aaa", 3.0);
		JAXBContext jaxbContext = JAXBContext.newInstance(StatLigne.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		// jaxbMarshaller.marshal(sl, f);
		jaxbMarshaller.marshal(sl, System.out);
		assertTrue(true);

		
	}

}
