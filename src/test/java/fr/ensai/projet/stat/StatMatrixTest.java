package fr.ensai.projet.stat;

import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.Before;
import org.junit.Test;

import fr.ensai.projet.metier.Cell;
import fr.ensai.projet.metier.Colonne;
import fr.ensai.projet.metier.Comment;
import fr.ensai.projet.metier.DataType;
import fr.ensai.projet.metier.Matrix;

public class StatMatrixTest {
	Matrix matrice1;
	StatMatrix stma;

	@Before
	public void setUp() throws Exception {
		// CREATION DE COMMENTAIRES
		Comment com1 = new Comment("Ceci est un commentaire visible",
				"Auteur 1", true);
		Comment com2 = new Comment("Ceci est un commentaire invisible",
				"Auteur 2", false);
		Comment com3 = new Comment("Et s'il y a plusieurs commentaires ...",
				"Auteur 3", true);
		ArrayList<Comment> coms = new ArrayList<Comment>();
		coms.add(com1);
		coms.add(com2);
		coms.add(com3);

		// CREATION DE CELLULES

		ArrayList<Cell<?>> cx1 = new ArrayList<Cell<?>>();
		Cell<String> c01 = new Cell<String>(null, "Brand Model",
				DataType.Header);
		cx1.add(c01);
		Cell<String> c11 = new Cell<String>(null, "Apple iPhone 5S",
				DataType.Texte);
		cx1.add(c11);
		Cell<String> c21 = new Cell<String>(null, "Blackberry Q10",
				DataType.Texte);
		cx1.add(c21);
		Cell<String> c31 = new Cell<String>(null, "Samsung Galaxy S4",
				DataType.Texte);
		cx1.add(c31);
		Cell<String> c41 = new Cell<String>(coms, "Sony Xperia L",
				DataType.Texte);
		cx1.add(c41);
		Cell<String> c51 = new Cell<String>(null, "Nokia Lumia 920",
				DataType.Texte);
		cx1.add(c51);

		ArrayList<Cell<?>> cx2 = new ArrayList<Cell<?>>();
		Cell<String> c02 = new Cell<String>(null, "RAM (GB)", DataType.Header);
		cx2.add(c02);
		Cell<Integer> c12 = new Cell<Integer>(null, 1, DataType.Entier);
		cx2.add(c12);
		Cell<Integer> c22 = new Cell<Integer>(null, 2, DataType.Entier);
		cx2.add(c22);
		Cell<Integer> c32 = new Cell<Integer>(null, 2, DataType.Entier);
		cx2.add(c32);
		Cell<Integer> c42 = new Cell<Integer>(null, 2, DataType.Entier);
		cx2.add(c42);
		Cell<Integer> c52 = new Cell<Integer>(null, 1, DataType.Entier);
		cx2.add(c52);

		ArrayList<Cell<?>> cx3 = new ArrayList<Cell<?>>();
		Cell<String> c03 = new Cell<String>(null, "OS", DataType.Header);
		cx3.add(c03);
		Cell<String> c13 = new Cell<String>(null, "iOS 7.1", DataType.Texte);
		cx3.add(c13);
		Cell<String> c23 = new Cell<String>(null, "Blackberry 10",
				DataType.Texte);
		cx3.add(c23);
		Cell<String> c33 = new Cell<String>(null, "Android 4.2.2",
				DataType.Texte);
		cx3.add(c33);
		Cell<String> c43 = new Cell<String>(null, "Android 4.2.2",
				DataType.Texte);
		cx3.add(c43);
		Cell<String> c53 = new Cell<String>(null, "Windows Phone 8.1",
				DataType.Texte);
		cx3.add(c53);

		ArrayList<Cell<?>> cx4 = new ArrayList<Cell<?>>();
		Cell<String> c04 = new Cell<String>(null, "Full Keyboard",
				DataType.Header);
		cx4.add(c04);
		Cell<Boolean> c14 = new Cell<Boolean>(null, false, DataType.Booleen);
		cx4.add(c14);
		Cell<Boolean> c24 = new Cell<Boolean>(null, true, DataType.Booleen);
		cx4.add(c24);
		Cell<Boolean> c34 = new Cell<Boolean>(null, false, DataType.Booleen);
		cx4.add(c34);
		Cell<Boolean> c44 = new Cell<Boolean>(null, false, DataType.Booleen);
		cx4.add(c44);
		Cell<Boolean> c54 = new Cell<Boolean>(null, false, DataType.Booleen);
		cx4.add(c54);

		// CREATION DE COLONNES
		ArrayList<Colonne> mat1 = new ArrayList<Colonne>();
		Colonne col1 = new Colonne(DataType.Texte, cx1);
		mat1.add(col1);
		Colonne col2 = new Colonne(DataType.Entier, cx2);
		mat1.add(col2);
		Colonne col3 = new Colonne(DataType.Texte, cx3);
		mat1.add(col3);
		Colonne col4 = new Colonne(DataType.Booleen, cx4);
		mat1.add(col4);

		// CREATION DE MATRICES
		matrice1 = new Matrix(mat1, "Matrice 1");
		
		// CREATION DE LA STATMATRIX
		stma = new StatMatrix(matrice1);

	}

	
	@Test
	public void testExport() throws JAXBException {
		stma = new StatMatrix(matrice1);


		JAXBContext jaxbContext = JAXBContext.newInstance(StatMatrix.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		//jaxbMarshaller.marshal(pcm, new File("test.xml"));
		jaxbMarshaller.marshal(stma, System.out);
	}
	
	
}
