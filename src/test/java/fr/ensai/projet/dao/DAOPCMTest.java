package fr.ensai.projet.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ensai.projet.metier.MatriceTestEnDur;
import fr.ensai.projet.metier.PCM;
import fr.ensai.projet.persistance.DAOPCM;
import fr.ensai.projet.persistance.PCMException;

public class DAOPCMTest {
	@PersistenceContext(unitName = "pcmeditor-test")
	private EntityManager em;

	private DAOPCM daopcm;

	PCM pcm;
	MatriceTestEnDur mted;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		daopcm = new DAOPCM(em);

		/**
		 * Etant donne un contexte; on sollicite le système (appel methode);
		 * then les assertions
		 */

	}

	@Test
	public void testWrite() throws PCMException {
		// Given
		mted = new MatriceTestEnDur();
		pcm = mted.getPcmTest();

		// When
		daopcm.create(pcm);

		// Then
		Assert.assertNotNull(pcm.getId());
		PCM pcmPersiste = em.find(PCM.class, pcm.getId());
		Assert.assertNotNull(pcmPersiste);
	}

	@Test
	public void testRead() throws PCMException {

		// given
		mted = new MatriceTestEnDur();
		pcm = mted.getPcmTest();

		// when
		daopcm.create(pcm);

		Assert.assertNotNull(pcm.getId());
		PCM pcmPersiste = em.find(PCM.class, pcm.getId());
		Assert.assertNotNull(pcmPersiste);

	}

	@Test
	public void testErase() throws PCMException {

		// given
		mted = new MatriceTestEnDur();
		pcm = mted.getPcmTest();

		// when
		daopcm.read(pcm.getId());

		//	daopcm.remove(pcm);
		Assert.assertNotNull(pcm);
		Assert.assertNull(em.find(PCM.class, pcm.getId()));
	}

}
