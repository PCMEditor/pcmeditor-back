package fr.ensai.projet.metier;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Matrice : ArrayList de colonnes
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="Matrix")
public class Matrix {
	
	// Attributs

	@Column(name="identMatrix")
	@Id	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlAttribute
	private int id;
	
	@JoinColumn(name="columns")
	@OneToMany(cascade = CascadeType.ALL)
	@XmlElement
	private List<Colonne> colonnes;

	@XmlAttribute
	private int nbCol;
	
	@Column(name="name")
	@XmlAttribute
	private String nameMatrix;

	// Constructeur
	public Matrix(ArrayList<Colonne> mat1, String nameMatrix) {
		super();
		this.colonnes = mat1;
		this.nameMatrix = nameMatrix;
		this.nbCol =  mat1.size();
	}
	public Matrix() {
		super();
	}
	
	//Getters et Setters 
	public String getNameMatrix() {
		return nameMatrix;
	}

	public void setNameMatrix(String nameMatrix) {
		this.nameMatrix = nameMatrix;
	}
	public List<Colonne>getColmuns() {
		return colonnes;
	}

	public void setColumns(ArrayList<Colonne> colmuns) {
		this.colonnes = colmuns;
		this.nbCol = colmuns.size();
	}

	
}
