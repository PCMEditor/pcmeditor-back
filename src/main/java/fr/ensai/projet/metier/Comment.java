package fr.ensai.projet.metier;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Commentaire
 */
@XmlRootElement
@Entity
@Table(name="Comment")
public class Comment {
	// Attributsid
	
	@Column(name="identComment")
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="content")
	private String content;
	@Column(name="author")
	private String author;
	@Column(name="date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	@Column(name="visible")
	private Boolean visible;
	
	
	// Concstructeur
	public Comment(String content, String author, boolean visible) {
		super();
		this.content = content;
		this.author = author;
		this.visible = visible;
	}
	
	public Comment() {
		super();
	}
	
	// Getters - Setters	
	public String getContent() {
		return content;
	}
	@XmlAttribute
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	@XmlAttribute
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getDate() {
		return date;
	}
	@XmlAttribute
	public void setDate(Date date) {
		this.date = date;
	}
	public Boolean isVisible() {
		return visible;
	}
	@XmlAttribute
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	
	
}
