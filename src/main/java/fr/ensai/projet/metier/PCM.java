package fr.ensai.projet.metier;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ensai.projet.marshallPCM.XMLException;
import fr.ensai.projet.marshallPCM.XmlPCMBackToFront;
import fr.ensai.projet.persistance.PCMException;

/**
 * PCM : ArrayList de matrix
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "PCM")
public class PCM {

	// Attributs
	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "identPCM")
	@XmlAttribute
	int id;
	
	@JoinColumn(name = "Matrices")
	@OneToMany(cascade = CascadeType.ALL)
	@XmlElement
	private List<Matrix> matrices;
	
	@Column(name = "name")
	@XmlAttribute
	private String namePCM;

	// COnstructeur
	public PCM(List<Matrix> matrices, String namePCM) {
		super();
		this.matrices = matrices;
		this.namePCM = namePCM;
	}

	public PCM() {
		super();
	}

	// Getters et Setters

	public String getNamePCM() {
		return namePCM;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setNamePCM(String namePCM) {
		this.namePCM = namePCM;
	}

	public List<Matrix> getMatrices() {
		return matrices;
	}

	public void setMatrices(ArrayList<Matrix> matrices) {
		this.matrices = matrices;
	}

	// METHODES

	/**
	 * Exports PCM to specified file
	 * 
	 * @param f
	 * @throws PCMException 
	 * 
	 */
	public void export(File f) throws XMLException {
		XmlPCMBackToFront classxml = new XmlPCMBackToFront();
		classxml.export(this, f);
	}

}
