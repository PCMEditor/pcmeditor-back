package fr.ensai.projet.metier;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Cellule métier ayant un contenu, un type ainsi qu'une Arraylist de commentaires
 * T est un type java (Int, bool, String, ...) contrairement aux DataType qui sont un Enum
 * 
 * @param <T> type de valeur.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "Cell")
public class Cell<T> {
	// Attributs
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "identCell")
	@XmlTransient
	private int id;
	@JoinColumn(name = "comments")
	@OneToMany(cascade = CascadeType.ALL)
	@XmlElement
	private List<Comment> comments;
	@Column(name = "content")
	@XmlElement
	private T content;
	@Column(name = "type")
	@XmlTransient
	private DataType type;

	// Constructeur
	public Cell(List<Comment> comments, T content, DataType type) {
		super();
		this.comments = comments;
		this.content = content;
		this.type = type;
	}

	public Cell() {
		super();

	}

	// Getters Setters
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}

	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

	public DataType getType() {
		return type;
	}

	public void setType(DataType type) {
		this.type = type;
	}

	// Méthodes

	/**
	 * Ajout d'un commentaire
	 * 
	 * @return void
	 */

	public void addComment() {

	}

}
