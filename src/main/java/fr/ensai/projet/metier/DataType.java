package fr.ensai.projet.metier;
/**
 * Type de la cellule, utile pour les stats.
 */
public enum DataType {
	Texte,
	Entier,
	Nombre,
	Booleen,
	Header,
	Extra;
}
