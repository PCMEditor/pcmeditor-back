package fr.ensai.projet.metier;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * Column, prenant une ArrayList de cellules et possédant un type.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="Column")
public class Colonne {
	// Attributs
	@Column(name="identColumn")
	@Id
	@XmlTransient
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="type")
	@XmlAttribute
	private DataType type;
	@JoinColumn(name="cells")
	@ManyToMany( cascade = CascadeType.ALL)
	@XmlElement
	private List<Cell<?>> cells;
	
	@XmlAttribute
	private String nomcol;
	
	// Constructeur
	public Colonne(DataType type, ArrayList<Cell<?>> cx2) {
		super();
		this.type = type;
		this.cells = cx2;
		this.nomcol = (String) cx2.get(0).getContent();
	}
	public Colonne() {
		super();
	}
	
	// Getters Setters
	public DataType getType() {
		return type;
	}
	public void setType(DataType type) {
		this.type = type;
	}
	public List<Cell<?>> getCells() {
		return cells;
	}
	public void setCells(ArrayList<Cell<?>> cells) {
		this.cells = cells;
		this.nomcol = (String) cells.get(0).getContent();
	}
	
	
}
