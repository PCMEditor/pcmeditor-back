package fr.ensai.projet.marshallPCM;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import fr.ensai.projet.metier.PCM;

/**
 * Import d'un XML en PCM
 * @author Groupe ALEJ
 *
 */
public class XmlPCMFrontToBack {

	/**
	 * Saves pcmExport as XML to file f and prints XML to standard output
	 * 
	 * @param pcmExport
	 * @param f
	 * @throws PCMException 
	 */

	public PCM importer(File f) throws XMLException{
		PCM p = new PCM();
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(PCM.class);
			Unmarshaller jaxbunMarshaller = jaxbContext.createUnmarshaller();
			p = (PCM) jaxbunMarshaller.unmarshal(f);
		 } catch (JAXBException e) {
	    	  	throw new XMLException("Erreur à l'import du fichier XML. \n Importez-vous un fichier issu de notre application ?");
	      }
		return p;
		
	}
}
