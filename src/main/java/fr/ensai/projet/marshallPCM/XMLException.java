package fr.ensai.projet.marshallPCM;

/**
 * Exception appelée en cas d'erreur du (un)marshall d'une PCM en XML
 * @author Groupe ALEJ
 *
 */
@SuppressWarnings("serial")
public class XMLException extends Exception {
	public XMLException(String descriptionErreur) {
		System.out.println("Erreur lors de la creation d'une PCM "
				+ descriptionErreur);
	}
}
