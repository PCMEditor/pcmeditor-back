package fr.ensai.projet.marshallPCM;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import fr.ensai.projet.metier.PCM;

/**
 * Export d'un PCM en XML
 * @author Groupe ALEJ
 *
 */
public class XmlPCMBackToFront {
	
	/**
	 * Saves pcmExport as XML to file f and prints XML to standard output
	 * 
	 * @param pcmExport
	 * @param f
	 * @throws XMLException 
	 */
	
	public void export(PCM pcmExport, File f) throws XMLException{
		

		
		try {
			 
			JAXBContext jaxbContext = JAXBContext.newInstance(PCM.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
			jaxbMarshaller.marshal(pcmExport, f);
			//jaxbMarshaller.marshal(pcmExport, System.out);
	 
		      } catch (JAXBException e) {
		    	  	throw new XMLException("Erreur à l'export du fichier XML. \n Oups, nous avons mal codé une fonction. Avertissez-nous et on répare ca très vite !");
		      }
	 
		}
	
}
