	package fr.ensai.projet.stat;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ensai.projet.metier.Cell;
import fr.ensai.projet.metier.Colonne;
import fr.ensai.projet.metier.Matrix;

/**
 * 
 * Cette classe s'occupera de dresser les statistiques sur une matrice
 * individuelle lors de sa construction. Plus spécifiquement, elle va instancier 
 * un ensemble de statlignes et calculer des statistiques dessus.
 * 
 * @author nicolas
 *
 */
@XmlRootElement(name="StatMatrix")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatMatrix {

	// Attributes
	/** See StatLigne documentation */
	@XmlElementWrapper(name="StatLignes")
	@XmlElement(name="StatLigne")
	private List<StatLigne> lignes;

	// Constructeur

	public StatMatrix() {
	}

	StatMatrix(Matrix m) {

		int np=0;
		int nf=0;
		int nce=0;
		int nco=0;
		lignes = new ArrayList<StatLigne>();

		
		
		for (Colonne c : m.getColmuns()) {
		
			np=0;
			nf++;
			for (Cell<?> ce : c.getCells()) {
				nce++;
				np++;
				if (ce.getComments() != null) {
					nco += ce.getComments().size();
				}
			}

		}
		lignes.add(new StatLigne("Number of products", np));
		lignes.add(new StatLigne("Number of features", nf));
		lignes.add(new StatLigne("Number of cells", nce));
		lignes.add(new StatLigne("Number of comments", nco));

		
	}

	public List<StatLigne> getLignes() {
		return lignes;
	}

	public void setLignes(List<StatLigne> lignes) {
		this.lignes = lignes;
	}



	// methods 




}
