package fr.ensai.projet.stat;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ensai.projet.metier.Matrix;
import fr.ensai.projet.metier.PCM;

/***
 * 
 * 
 * Cette classe construira une représentation statistique d'une PCM qui lui est
 * passée lors de son initialisation
 * 
 * @author ensai
 *
 */

@XmlRootElement(name = "statPCM")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatPCM {

	@XmlElementWrapper(name = "StatGlobal")
	@XmlElement(name = "StatLigne")
	private List<StatLigne> lignesGlobal;

	@XmlElementWrapper(name = "StatMatrices")
	@XmlElement(name = "StatMatrix")
	private List<StatMatrix> stmt;

	// Constructeur

	public StatPCM() {
	}

	public StatPCM(PCM pcm) {
		// Eventuellement initialiser les arraylist<integer> à 0
		stmt = new ArrayList<StatMatrix>();
		lignesGlobal = new ArrayList<StatLigne>();
		int nm = 0;
		int tnp = 0;
		int tnf = 0;
		int tnce = 0;
		int tnco = 0;

		for (Matrix m : pcm.getMatrices()) {
			StatMatrix stm = new StatMatrix(m);
			stmt.add(stm);
			tnp += stm.getLignes().get(0).getValue();
			tnf += stm.getLignes().get(1).getValue();
			tnce += stm.getLignes().get(2).getValue();
			tnco += stm.getLignes().get(3).getValue();
			nm++;
		}
		lignesGlobal.add(new StatLigne("Number of matrices", nm));
		lignesGlobal.add(new StatLigne("Total number of products", tnp));
		lignesGlobal.add(new StatLigne("Total number of features", tnf));
		lignesGlobal.add(new StatLigne("Total number of cells", tnce));
		lignesGlobal.add(new StatLigne("Total number of products", tnco));

	}


}
