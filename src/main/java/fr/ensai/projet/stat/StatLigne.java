package fr.ensai.projet.stat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * Une ligne représente le couplage entre un nom de statistique et une valeur
 * calculée. Les calculs sont délégués à la statmatrix, car elle a à sa
 * disposition toutes les informations dont on est suceptible de se servir pour
 * effectuer des statistiques (schema individu variable cohérent)
 * 
 * @author ensai
 *
 */

@XmlRootElement(name = "line")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatLigne {

	// attr
	/** Le nom de la statistique */
	@XmlElement(name = "name")
	private String name;

	/** La valeur de la statistique */
	@XmlElement(name = "value")
	private double value;

	// cstr
	/**
	 * Constructeur vide (JAXB)
	 */
	public StatLigne() {
	}

	/**
	 * Constructeur exhaustif
	 * @param name nom
	 * @param value valeur
	 */
	public StatLigne(String name, double value) {
		this.name = name;
		this.value = value;
	}

	// Getters, Setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	// mth


}
