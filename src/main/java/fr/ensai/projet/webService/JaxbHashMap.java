package fr.ensai.projet.webService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * Cette classe est un wrapper de hashap, qui a pour but de permettre la
 * s�rialisation/d�s�rialisation en JAX d'une classe de type hashmap<k,v>
 * 
 * 
 * @author ensai
 *
 * @param <K>
 *            le type de la cl� (doit impl�menter hashcode())
 * @param <V>
 *            le type de la valeur
 */
@XmlRootElement(name = "HashMap")
public class JaxbHashMap<K, V> {
	protected HashMap<K, V> hashMap;

	/**
	 * Constructeur par d�faut, n�cessaire pour le fonnctionnement de JAX
	 */
	public JaxbHashMap() {
	}

	/**
	 * Constructeur complet
	 * 
	 * @param hashMap l'objet Hashmap que l'on souhaite (d�)s�rialiser
	 */
	public JaxbHashMap(HashMap<K, V> hashMap) {
		this.hashMap = hashMap;
	}

	
	/**
	 * Getter sur les cl�s 
	 * 
	 * @return l'ensemble des cl�s de la hashmap
	 * (un Set car tous ses �l�ments sont uniques)
	 */
	@XmlElement(name = "key")
	public Set<K> getHashMapKeys() {
		return hashMap.keySet();
	}

	/**
	 * Getter sur les valeurs
	 *  
	 * @return une ArrayList de toutes les valeurs
	 * de la hashmap
	 */
	@XmlElement(name = "values")
	public List<V> getHashMapValues() {
		List<V> vals = new ArrayList<V>();
		for (K key : hashMap.keySet()) {
			vals.add(hashMap.get(key));
		}

		return vals;
	}

}