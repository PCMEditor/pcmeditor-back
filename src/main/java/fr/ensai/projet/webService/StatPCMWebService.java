package fr.ensai.projet.webService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.ensai.projet.metier.PCM;
import fr.ensai.projet.persistance.DAOPCM;
import fr.ensai.projet.persistance.PCMException;
import fr.ensai.projet.stat.StatPCM;

@Path("/statpcm")
@Stateless
public class StatPCMWebService {

	@EJB
	DAOPCM daopcm;

	@GET
	public String hello() {
		return "ok";
	}

	/**
	 * Renvoie un objet m�tier StatXml correspondant � la PCM dont l'identifiant
	 * est pass� en parametre. Cette
	 * 
	 * @param idPCM
	 * @return
	 */
	@GET
	@Path("/{idPCM}")
	@Produces({ MediaType.APPLICATION_XML })
	public Response recuperer(@PathParam("idPCM") int idPCM) {

		PCM pcm;
		try {
			pcm = daopcm.read(idPCM);
			StatPCM stpcm = new StatPCM(pcm);
			System.out
					.println("StatPCMWebService.recuperer() : PCM trouv�e, StatPCM g�n�r�e");
			return Response.status(201).entity(stpcm).build();

		} catch (PCMException e) {

			// La PCM demand�e n'est pas en base: erreur HTTP 404 not found
			System.out
					.println("StatPCMWebService.recuperer() : pas de PCM associ�e ");
			return Response.status(404).build();

		}

	}

}
