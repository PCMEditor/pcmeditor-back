package fr.ensai.projet.webService;

import java.util.HashMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.ensai.projet.metier.PCM;
import fr.ensai.projet.persistance.DAOPCM;
import fr.ensai.projet.persistance.PCMException;

/**
 * 
 * Cette classe impl�menente le CRUD (Create, Read, Update, Delete) cot�
 * Web-service en REST pour des objets m�tier du projet (PCM et donc Matrix, ..
 * 
 * ses services sont disponibles � partir de /pcm
 * 
 * 
 * 
 * 
 * 
 * @author ensai
 *
 */
@Path("/pcm")
@Stateless
public class PCMWebService {

	/**
	 * daopcm est une classe qui fait l'interface entre les POJO et la
	 * persistence. Puisque nous n'avons pas de r�gles de gestion avanc�es (qui
	 * se limitent � un contr�le de saisie tout au plus), il n'y a pas
	 * d'avantages ici � mettre en place une classe interm�diaire entre les les
	 * webservices et la persistence.
	 * 
	 */
	@EJB
	DAOPCM daopcm;

	/**
	 *
	 * 
	 * Permet d'initaliser la base h2
	 * 
	 */
	@GET
	@Path("/init")
	public Response init() {
		try {
			daopcm.init();
		} catch (PCMException e) {
			System.out
					.println("PCMWebService.init() : Erreur dans l'initialization de la BDD");
			e.printStackTrace();
			return Response.status(404).build();
		}
		return Response.ok().build();
	}

	/**
	 * Permet d'obtenir une liste index�e <identifiant, nomPCM>
	 * 
	 * Appel�e par un GET sur /pcm
	 * 
	 * @return Une r�ponse HTTP "ok" avec un fichier application/xml qui
	 *         repr�sente une Hashmap index�e par les identifiants de PCM connus
	 *         en base et dont les valeurs sont les noms correspondants � ces
	 *         PCM
	 */
	@GET
	@Produces({ MediaType.APPLICATION_XML })
	public Response listerIdPCM() {
		HashMap<Integer, String> list = new HashMap<Integer, String>();
		list = daopcm.listerId();
		JaxbHashMap<Integer, String> jhm = new JaxbHashMap<Integer, String>(
				list);
		return Response.ok().entity(jhm).build();

	}

	/**
	 * 
	 * Permet d'obtenir une PCM s�rialis�e en JAXB � partir de son identifiant,
	 * en appelant le service web en GET sur /pcm/{identifiantPCM}
	 * 
	 * 
	 * @param l
	 *            'identifiant de la PCM (valeur enti�re)
	 * @return une r�ponse HTTP, soit 404 not found, soit 201 object created,
	 *         avec un content-type application/xml, repr�sentation JAXB de la
	 *         classe m�tier correspondante
	 */

	@GET
	@Path("/{idPCM}")
	@Produces({ MediaType.APPLICATION_XML })
	public Response read(@PathParam("idPCM") int idPCM) {

		try {
			PCM pcm = daopcm.read(idPCM);

			System.out.println("PCMWebService.read() : PCM " + pcm.toString()
					+ " r�cup�r�e (metier -> webservice)");

			// HTTP 201 objet cr�e
			return Response.status(201).entity(pcm).build();

		} catch (PCMException e) {
			e.printStackTrace();
			// cas erreur : �chec du chargement, on retourne une erreur HTTP
			return Response.status(404).build();

		}

	}

	/**
	 * Permet d'�crire en base de donn�es un objet m�tier de type PCM(s�rialis�
	 * avec les bindings JAX) pass� lors d'une requ�te POST � notre web service
	 * 
	 * @param pcm
	 *            l'objet PCM (la d�s�rialisation nous est transparente)
	 * 
	 * @return l'objet PCM �ventuellement modifi� qui a �t� persist� en base
	 */
	@POST
	@Consumes({ MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_XML })
	public Response create(PCM pcm) {
		try {
			daopcm.create(pcm);
		} catch (PCMException e) {
			// Le service n'a pas abouti � la cr�ation d'une PCM
			// Erreur 400 : la requete n'est pas bonne
			return Response.status(400).build();
		}
		System.out.println("PCMWebService.create() + PCM" + pcm.toString()
				+ "rajout�e webservice -> metier");
		// HTTP 201 objet cr�e
		return Response.status(201).entity(pcm).build();
	}

	/**
	 * Permet de mettre � jour un objet de type PCM dans une base de donn�es
	 * (par exemple, lors de l'�dition d'un commentaire) gr�ce � l'appel de PUT
	 * sur
	 * 
	 * 
	 * @param pcm
	 * @return
	 */
	@PUT
	@Consumes({ MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_XML })
	public Response update(PCM pcmParam) {
		System.out.println("PCMWebService.update() call received for pcm "
				+ pcmParam.getId());
		try {
			daopcm.modify(pcmParam);
			// daopcm.persister(pcm);
		} catch (PCMException e) {
			// Le service n'a pas abouti � la cr�ation d'une PCM
			// Erreur 400 : la requete n'est pas bonne
			return Response.status(400).build();
		}
		// HTTP 201 objet cr�e
		return Response.status(201).entity(pcmParam).build();
	}

	/**
	 * 
	 * Efface la PCM index�e par @param idPCM de la base.
	 * 
	 * @param idPCM
	 * @return
	 */
	@DELETE
	@Path("/{idPCM}")
	public Response delete(@PathParam("idPCM") int idPCM) {
		daopcm.remove(idPCM);
		// retourne HTTP 200 "ok"
		return Response.status(200).build();

	}

}
