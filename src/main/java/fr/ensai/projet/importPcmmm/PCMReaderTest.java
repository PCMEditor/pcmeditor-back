package fr.ensai.projet.importPcmmm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pcmmm.Constraint;
import pcmmm.PCM;
import pcmmm.ValuedCell;
import fr.ensai.projet.marshallPCM.XmlPCMFrontToBack;
import fr.ensai.projet.metier.DataType;



/**
 * Test du lecteur de PCMMM
 * @author 
 *
 */
public class PCMReaderTest {
	
	private static Logger _LOGGER = Logger.getLogger("PCMReaderTest");
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}
	
	//@Test
	public void testRecuperationRowetCol() {
		
		String fileName = "Comparison_of_3D_computer_graphics_software.pcm" ;
		PCM p = new PCMReader().getPCM(fileName);
		_LOGGER.info(p.getMatrices().get(0).getName());
		assertEquals(p.getMatrices().size(), 3, 0.0); // one matrix
		assertEquals(p.getMatrices().get(0).getCells().get(0).getRow(), 0, 0.0);
		assertEquals(p.getMatrices().get(0).getCells().get(0).getColumn(), 1, 0.0);		
	}
	
	//@Test
	public void testRecuperationTypeCellulePCMMM() {
		
		String fileName = "Comparison_of_Colorado_ski_resorts.pcm" ;
		PCM p = new PCMReader().getPCM(fileName);
		_LOGGER.info(p.getMatrices().get(0).getName());
		_LOGGER.info(((Constraint)((ValuedCell) p.getMatrices().get(0).getCells().get(36)).getInterpretation()).getClass().getName());
		assertTrue("pcmmm.impl.IntegerImpl".equals(((Constraint)((ValuedCell) p.getMatrices().get(0).getCells().get(36)).getInterpretation()).getClass().getName()));
		
	}
	
	//@Test
	public void testRecuperationMatriceEntiere(){
		String fileName = "Comparison_of_3D_computer_graphics_software.pcm" ;
		fr.ensai.projet.metier.PCM p = PCMReaderApp.recupererPCMrest(fileName,false);
		File f = new File("testRecupMatNew.xml");
		try {
			p.export(f);
		assertTrue(true);
			}catch(Exception e){
		e.printStackTrace();
		assertTrue(false);
			}
	}	
	
	//@Test
	public void testImport(){
		File f = new File("testRecupMatNew.xml");
		XmlPCMFrontToBack xml = new XmlPCMFrontToBack();
		
		try {
		fr.ensai.projet.metier.PCM	p = xml.importer(f);
		assertTrue(true);
		assertEquals(p.getNamePCM(), "Comparison of 3D computer graphics software");
		assertEquals(p.getMatrices().get(0).getNameMatrix(),"General information");
		assertEquals(p.getMatrices().get(0).getColmuns().get(0).getCells().get(0).getContent(),"Application");
		assertEquals(p.getMatrices().get(0).getColmuns().get(0).getType(), DataType.Header);		
			}catch(Exception e){
		e.printStackTrace();
		assertTrue(false);
			}
	}	
	
	@Test
	public void testRecupNomsMatrice(){
		String dir = "input/";
		List<String> results = (List<String>) PCMReaderApp.getPCMFileNames(dir);
		PCMReaderApp.getPCMFileNamesIndex(dir);
		_LOGGER.info(results.toString());
	}
		

	
	
}