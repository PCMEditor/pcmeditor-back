package fr.ensai.projet.importPcmmm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import pcmmm.Cell;
import pcmmm.Constraint;
import pcmmm.Matrix;
import pcmmm.PCM;
import pcmmm.ValuedCell;
import fr.ensai.projet.metier.DataType;

/**
 * Classe de lien entre pcmmm et pcm métier
 * @author Groupe ALEJ
 *
 */
public class PCMReaderApp {

	/**
	 * Transforme des PCMmm issues du modèle ecore défini par macher en PCM fonctionelle définie par notre métier
	 * 
	 * @param Nom du fichier à récupérer
	 * @param True si matrices à aller chercher en base persistée, false si stockées localement
	 * @return PCM métier
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static fr.ensai.projet.metier.PCM recupererPCMrest(String filename,boolean persistance){
		// Récupération de la PCM
		PCM p;
		if(persistance) {
			p = new PCMReader("http://localhost:8080/PCMEditor/input/").getPCMrest(filename);
		} else {
			p = new PCMReader().getPCM(filename);
		}
		// Création de l'arraylist de Matrices métier
		ArrayList<fr.ensai.projet.metier.Matrix> almat = new ArrayList<fr.ensai.projet.metier.Matrix>();
		// Boucle de récupération des matrices
		for(int i=0;i < p.getMatrices().size();i++){
			// Récupération de la matrice Pcmmm
			Matrix mat = p.getMatrices().get(i);
			// Récupération de ses cellules
			EList<Cell> cells =  mat.getCells();
			// Définition nombres de cellules et colonnes
			int nbcol=0;
			int nbrow = 0;
			for (Cell cell : cells){
				if (cell.getColumn()+1 > nbcol){nbcol=cell.getColumn()+1;}
				if (cell.getRow()+1> nbrow){nbrow=cell.getRow()+1;}

			}

			ArrayList<fr.ensai.projet.metier.Colonne> alcol = new ArrayList<fr.ensai.projet.metier.Colonne>();

			// Gestion du cas de la première colonne (composée uniquement de Header)
			fr.ensai.projet.metier.Cell<?> [] tabcel = new fr.ensai.projet.metier.Cell<?> [nbrow];
			for (Cell cell : cells){
				// Création de l'ArrayList de Cellules Metier


				if(cell.getColumn()==0){
					//Création d'une cellule métier de type Header
					fr.ensai.projet.metier.Cell<?> cellmet = new fr.ensai.projet.metier.Cell(null,cell.getVerbatim(),DataType.Header);
					// Ajout dans l'AL de Cellules
					tabcel[cell.getRow()]=cellmet;

				}
			}
			// Création et ajout de la colonne métier à l'AL de colonnes métiers
			ArrayList<fr.ensai.projet.metier.Cell<?>> alcel = new ArrayList<fr.ensai.projet.metier.Cell<?>>(Arrays.asList(tabcel));
			fr.ensai.projet.metier.Colonne colMet = new fr.ensai.projet.metier.Colonne(DataType.Header, alcel);
			alcol.add(colMet);

			// Récupération des cellules selon leur numéro de colonne.
			for (int c=1;c < nbcol; c++){
				fr.ensai.projet.metier.Cell<?> [] tabcel2 = new fr.ensai.projet.metier.Cell<?> [nbrow];
				DataType datatype = null;
				DataType datatypecol = null;
				boolean datatypecolOK = true;
				for (Cell cell : cells){
					// Création de l'ArrayList de Cellules Metier
					// Si la cellule fait partie de la colonne à récuperer
					if (cell.getColumn() == c){
						// Gestion de la cellule Header
						if (cell.getRow()==0){
							fr.ensai.projet.metier.Cell<?> cellmet = new fr.ensai.projet.metier.Cell(null,cell.getVerbatim(),DataType.Header);
							tabcel2[0]=cellmet;
						}
						else{
							// Récupération du DataType de la colonne à partir du type de la première cellule
							if (cell.getRow()==1) {}
							// Création et ajout de la cellule métier à l'AL de cellules métiers
							fr.ensai.projet.metier.Cell cellmet = new fr.ensai.projet.metier.Cell(null,null,null);
							if (cell.getClass().toString().equals("class pcmmm.impl.HeaderImpl")){
								cellmet.setContent(cell.getVerbatim());
								datatype=DataType.Header;
							}else if (cell.getClass().toString().equals("class pcmmm.impl.ExtraImpl")){
								cellmet.setContent(cell.getVerbatim());
								datatype=DataType.Extra;
							}else{
								switch(((Constraint)((ValuedCell) cell).getInterpretation()).getClass().getName()){
								case("pcmmm.impl.BooleanImpl"):{
									datatype=DataType.Booleen;
									Boolean bool = false;
									if(cell.getVerbatim()=="Yes"){bool=true;}
									cellmet.setContent(bool);
									break;
								}
								case("pcmmm.impl.IntegerImpl"):{
									cellmet.setContent(Integer.parseInt(cell.getVerbatim()));
									datatype=DataType.Entier;break;
								}
								case("pcmmm.impl.DoubleImpl"):{
									try {
									cellmet.setContent(Double.parseDouble(cell.getVerbatim()));
									datatype=DataType.Nombre;
									} catch (NumberFormatException e){
									cellmet.setContent(cell.getVerbatim());
									datatype=DataType.Texte;	
									}
									break;
								}
								default:cellmet.setContent(cell.getVerbatim());datatype=DataType.Texte;
								}
							}
							cellmet.setType(datatype);
							tabcel2[cell.getRow()]=cellmet;
						}
						// Gestion du datatype de la coonne. On regarde la 1ere cellule et on compare les cellules suivantes. Si c'est le même type, nickel, sinon on considere que c'est du texte.
						if (cell.getRow()==1) {datatypecol=datatype;}
						else{if(datatypecolOK && datatype!=datatypecol){datatypecolOK=false;datatypecol=DataType.Texte;}}
					}
				}
				// Création et ajout de la colonne métier à l'AL de colonnes métiers
				ArrayList<fr.ensai.projet.metier.Cell<?>> alcel2 = new ArrayList<fr.ensai.projet.metier.Cell<?>>(Arrays.asList(tabcel2));
				fr.ensai.projet.metier.Colonne colMet2 = new fr.ensai.projet.metier.Colonne(datatypecol, alcel2);
				

				alcol.add(colMet2);
			}

			// Création et ajout de la matrice métier à l'AL de matrices métiers
			fr.ensai.projet.metier.Matrix matmet = new fr.ensai.projet.metier.Matrix(alcol,mat.getName());
			almat.add(matmet);
		}

		fr.ensai.projet.metier.PCM maPCM = new fr.ensai.projet.metier.PCM(almat,filename.replace("_", " ").substring(0, filename.length()-4));


		return maPCM;
	}


	/**
	 * Récupère nom des fichers présents dans un répertoire
	 * @param dir
	 * @return set of filenames contained in the directory dir
	 */

	public static Collection<String> getPCMFileNames(String dir) {


		File fDir = new File(dir);
		List<String> results = new ArrayList<String>();
		File[] files = fDir.listFiles(); 
		for (File file : files) {
			if (file.isFile()) {
				results.add(file.getName());
			}
		}
		return results ; 
	}

	/**
	 * Récupère nom des fichers présents dans un répertoire
	 * @param dir
	 * @return set of filenames contained in the directory dir
	 */

	public static void getPCMFileNamesIndex(String dir) {

		File fDir = new File(dir);
		File[] files = fDir.listFiles(); 
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter("index.txt"));
			for (File file : files) {
				if (file.isFile()) {
					writer.write(file.getName()+"\n");
				}
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


}
