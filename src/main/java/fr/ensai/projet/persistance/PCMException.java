package fr.ensai.projet.persistance;

import javax.ejb.Stateless;

/**
 * Exception renvoy�e lors d'un �venement impr�vu et ext�rieur qui nous empeche
 * de travailler correctement
 * 
 * @author ensai
 *
 */

@Stateless
public class PCMException extends Exception {
	private static final long serialVersionUID = 1L;

	public PCMException(String descriptionErreur) {
		System.out.println("Erreur lors de la creation d'une PCM "
				+ descriptionErreur);
	}
}