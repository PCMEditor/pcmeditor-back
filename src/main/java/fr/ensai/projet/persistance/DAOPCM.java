package fr.ensai.projet.persistance;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.ensai.projet.importPcmmm.PCMReaderApp;
import fr.ensai.projet.metier.PCM;

/**
 * 
 * 
 * Permettra l'exportation de PCM à travers des appels de DAOPCM<PCM>
 * 
 * 
 * @author nicolas
 *
 * @param <T>
 *            Doit contenir PCM
 */
@Stateless
public class DAOPCM {

	@PersistenceContext(unitName = "pcmeditor")
	private EntityManager em;

	public DAOPCM() {
		super();
	}

	public DAOPCM(EntityManager em) {
		super();
		this.em = em;
	}
	
	
	/** 
	 * Permet d'initaliser la base h2
	 * @throws PCMException 
	 * 
	 */
	
	public void init() throws PCMException{
	
		// ATTENTION
		// Pour faire marcher la fonction ci dessous, modifier le déployement assembly et rajouter model.jar	

		URL url;
			try {
				url = new URL("http://localhost:8080/PCMEditor/input/index.txt");
				Scanner s = new Scanner(url.openStream());
				while (s.hasNextLine()){
					String fileName = s.nextLine();
					fr.ensai.projet.metier.PCM p = PCMReaderApp.recupererPCMrest(fileName,true);
					create(p);
					System.out.println("Import du pcmmm " + p.getNamePCM());
				}
				s.close();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
			
	}

	/**
	 * 
	 * Retourne une hashmap index�e par l'identifiant de pcm et dont les valeurs
	 * correspondent au nom
	 * 
	 * Persistence -> Metier
	 * 
	 * @return
	 */
	public HashMap<Integer, String> listerId() {
		List<PCM> result = em.createQuery("SELECT pcm FROM PCM pcm", PCM.class)
				.getResultList();
		if (result.size() <= 0) {
			// System.out
			// .println("DAOPCM.listerId() : la BDD est vide; une NPE arrive !");
			System.out.println("DAOPCM.listerId() :  aucune PCM en base (l'initialisation se fait desormais en appelant /pcm/init en GET)");
		}
		HashMap<Integer, String> funcRes = new HashMap<Integer, String>();
		for (PCM p : result) {
			funcRes.put(p.getId(), p.getNamePCM());

		}
		return funcRes;

	}

	/**
	 * Persiste la pcm
	 * 
	 * POJO => Persistnce
	 * 
	 * @param pcm
	 * @throws PCMException
	 */
	public void create(PCM pcm) throws PCMException {

		if (em.find(pcm.getClass(), pcm.getId()) != null) {
			throw new PCMException(
					"Une PCM d�ja present en base porte le meme identifiant");
		}
		em.persist(pcm);
	}

	/**
	 * Load charge l'identifiant requis Persistence => POJO
	 * 
	 * @param id
	 *            l'identifiant que l'on veut
	 * @return la PCM que l'on veut retourner
	 * @throws PCMException
	 *             au cas ou que quelque chose d'ext�rieur nous empeche de
	 *             charger notre PCM correctement
	 */
	public PCM read(int id) throws PCMException {
		System.out.println("DAOPCM.load() Loading for idPCM = " + id
				+ " requested)");
		PCM res = null;
		res = em.find(PCM.class, id);
		if (res == null) {
			throw new PCMException(
					"Aucune PCM n'a �t� trouv�e portant cet identifiant en base");
		}

		return (PCM) res;
	}

	/**
	 * Modifie la PCM en base et retourne le bon
	 * 
	 * POJO => persistence => POJO TODO a implementer
	 * 
	 * @param pcm
	 *            La pcm a charger
	 * @return La PCM écrite en base
	 * @throws PCMException
	 *             erreur de nature ext�rieure au bon fonctionnement de notre
	 *             programme
	 */
	public PCM modify(PCM pcmParam) throws PCMException {
		System.out
				.println("DAOPCM.modify() : request for modification of PCM: "
						+ pcmParam.getId() + "received");

	
		em.merge(pcmParam);
		// on teste ensuite que la PCM requet�e sur l'identifiant de PCM de la
		// matrice pass�e en param�tre soient bien r�cup�rable en base de donn�e
		if (em.find(PCM.class, pcmParam.getId()) != null) {
			return pcmParam;
		} else {
			throw new PCMException("La requete de modification n'a pu aboutir");
		}

	}

	/**
	 * 
	 * 
	 * Efface la PCM passée en paramètre
	 * 
	 * POJO => boolean
	 * 
	 * @param idPCM
	 *            à effacer
	 * @return selon qu'il a bien effacé
	 */
	public boolean remove(int idPCM) {
		System.out.println("Removal for " + idPCM
				+ ". requested");
		em.remove(idPCM);
		return true;
	}

}
